package main.java.pl.sda;

import java.util.Scanner;
import java.time.LocalDate;
import java.time.Period;


public class NextMeeting {
    public static void main(String[] args) {

        LocalDate currentDay = LocalDate.now();
        System.out.println("Dzisiaj mamy " + currentDay);

        System.out.println("Kiedy będą następne zajęcia? (YYYY-MM-DD)");

        Scanner dateScanner = new Scanner(System.in);

        String dateOfMeeting = dateScanner.nextLine();

        LocalDate dateOfMeetingParsed = LocalDate.parse(dateOfMeeting);

        Period span = Period.between(currentDay, dateOfMeetingParsed);

        System.out.println("Następne zajęcia będą za " + span.getDays() + " dni");

    }
}
